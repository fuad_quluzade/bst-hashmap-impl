package hashmap_impl;

import java.util.Arrays;

public class CustomHashMap<K,V>{


    private int capacity = 16;

    private Node<K, V>[] node;

    public CustomHashMap(){
        node = new Node[capacity];
    }

    public CustomHashMap(int capacity){
        this.capacity = capacity;
        node = new Node[capacity];
    }

    private int index(K key){
        if(key == null){
            return 0;
        }
        return Math.abs(key.hashCode() % capacity);
    }

    public void put(K key, V value){
        int index = index(key);
        Node newEntry = new Node(key, value, null);
        if(node[index] == null){
            node[index] = newEntry;
        }else {
            Node<K, V> previousNode = null;
            Node<K, V> currentNode = node[index];
            while(currentNode != null){
                if(currentNode.getKey().equals(key)){
                    currentNode.setValue(value);
                    break;
                }
                previousNode = currentNode;
                currentNode = currentNode.getNext();
            }
            if(previousNode != null)
                previousNode.setNext(newEntry);
        }
    }

    public V get(K key){
        V value = null;
        int index = index(key);
        Node<K, V> entry = node[index];
        while (entry != null){
            if(entry.getKey().equals(key)) {
                value = entry.getValue();
                break;
            }
            entry = entry.getNext();
        }
        System.out.println(value);
        return value;
    }

    public void remove(K key){
        int index = index(key);
        Node previous = null;
        Node entry = node[index];
        while (entry != null){
            if(entry.getKey().equals(key)){
                if(previous == null){
                    entry = entry.getNext();
                    node[index] = entry;
                    return;
                }else {
                    previous.setNext(entry.getNext());
                    return;
                }
            }
            previous = entry;
            entry = entry.getNext();
        }
    }

    public void display(){
        for(int i = 0; i < capacity; i++){
            if(node[i] != null){
                Node<K, V> currentNode = node[i];
                while (currentNode != null){
                    System.out.println(String.format("{ %s ,  %s }", currentNode.getKey(), currentNode.getValue()));
//                    System.out.println("{"+currentNode.getKey()+",{"+currentNode.getValue()+"}");
                    currentNode = currentNode.getNext();
                }
            }
        }
    }

}
